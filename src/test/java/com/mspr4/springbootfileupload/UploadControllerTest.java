package com.mspr4.springbootfileupload;


import com.mspr4.springbootfileupload.controller.UploadController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@ExtendWith({MockitoExtension.class})
public class UploadControllerTest {
    @Mock
    UploadController uploaderController;

    @BeforeEach
    public void UploadControllerTest() {
    }

    @Test
    public void getFileNameExtenxionCorrect() throws Exception {
        File file = new File("test.xml");
        String extension = uploaderController.getFileNameExtenxion(file.getName());
        Assertions.assertEquals("xml", extension);
    }

    @Test
    public void getFileNameExtenxionIncorrect() throws Exception {
        Assertions.assertEquals(null, this.uploaderController.getFileNameExtenxion("test"));
    }
}