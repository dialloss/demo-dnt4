package com.mspr4.springbootfileupload.controller;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class UploadController {
    private static String UPLOADED_FOLDER = System.getProperty("java.io.tmpdir");
    private int minUploadSizeInMb = 104857600;

    public UploadController() {
    }

    @GetMapping({"/"})
    public String index() {
        return "upload";
    }

    public String getFileNameExtenxion(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf(46);
        if (i > 0) {
            extension = fileName.substring(i + 1);
        }

        return extension;
    }

    @PostMapping({"/upload"})
    public String singleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Veillez selectionner un fichier à charger");
            return "redirect:uploadStatus";
        } else {
            try {
                byte[] bytes = file.getBytes();
                Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
                Files.write(path, bytes, new OpenOption[0]);
                String fileName = file.getOriginalFilename();
                String extension = this.getFileNameExtenxion(fileName);
                if (!extension.equals("xml") && !extension.equals("xml")) {
                    redirectAttributes.addFlashAttribute("message", "Veuillez selectionner un fichier xml ");
                } else {
                    redirectAttributes.addFlashAttribute("message", "Le fichier '" + extension + "'a été chargé avec succès !");
                }
            } catch (IOException var7) {
                var7.printStackTrace();
            }

            return "redirect:/uploadStatus";
        }
    }

    @GetMapping({"/uploadStatus"})
    public String uploadStatus() {
        return "uploadStatus";
    }
}
