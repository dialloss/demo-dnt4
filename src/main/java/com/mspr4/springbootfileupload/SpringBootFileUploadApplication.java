package com.mspr4.springbootfileupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootFileUploadApplication {
	public SpringBootFileUploadApplication() {
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(SpringBootFileUploadApplication.class, args);
	}
}